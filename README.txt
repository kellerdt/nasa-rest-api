Requirements:
GIT, Maven 

Running Directions:

Option A:
Run the shell script 'run.sh'.
	- If the script is not runnable execute the following command (chmod a+x run.sh)
This should build the maven project and then run the Spring-boot application from your terminal.

Option B: (If A fails)
1. Import the maven project into your favorite IDE.
2. Run the (com.disney.nasa.NasaRestAPIStartup) class from within the IDE to start the server.


Testing:

After the server is running go to the following url:
http://localhost:8080/nasa/launch/launchday?startDate=2015-09-07&endDate=2015-09-14
