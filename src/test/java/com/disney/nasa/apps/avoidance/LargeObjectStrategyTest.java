package com.disney.nasa.apps.avoidance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.disney.nasa.models.NasaApiResult.Diameter;
import com.disney.nasa.models.NasaApiResult.NearEarthObject;

/**
 * Test that the strategy methods are working correctly.
 */
public class LargeObjectStrategyTest {

	LargeObjectStrategy los = new LargeObjectStrategy();
	
	@Test
	public void testNoObjectsReturnsZero() {
		List<NearEarthObject> l = new ArrayList<>();
		int count = los.countDangerousObjects(l);
		
		Assert.assertEquals(0, count);
	}
	
	@Test
	public void onlyOneObjectIsLarge() {
		List<NearEarthObject> l = new ArrayList<>();
		
		l.add(createNEO("Bob", 0.6));
		l.add(createNEO("Sally", 0.023));
		l.add(createNEO("Jack", 0.112));
		
		int count = los.countDangerousObjects(l);
		Assert.assertEquals(1, count);
	}
	
	private NearEarthObject createNEO(String name, double diameter) {
		NearEarthObject n = new NearEarthObject();
		n.name = name;
		Diameter d = new Diameter();
		d.estimated_diameter_max = diameter;
		Map<String,Diameter> dm = new HashMap<>();
		dm.put(LargeObjectStrategy.KILOS_KEY, d);
		n.estimated_diameter = dm;
		return n;
	}
}
