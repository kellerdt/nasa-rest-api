package com.disney.nasa.apps;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import junit.framework.TestCase;

/**
 * Test the individual responses of the NASA app without having to deal with
 * the specifics of the REST server.
 */
public class NasaNEOFeedTest extends TestCase {

	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	NasaNEOFeed app;
	
	public NasaNEOFeedTest(String name) {
		super(name);
		app = new NasaNEOFeed();
	}

	@Test
	public void testValidDate() throws ParseException {
		Date startDate = df.parse("2015-09-07");
		Date endDate = df.parse("2015-09-08");
		
		String launchDate = app.findSafestDay(startDate, endDate);
		
		System.out.println(launchDate);
		Assert.assertEquals(launchDate, "2015-09-08");
	}
	
	@Test
	public void testEndDateBeforeStartDate() throws ParseException {
		Date endDate = df.parse("2015-09-07");
		Date startDate = df.parse("2015-09-08");
		
		String launchDate = app.findSafestDay(startDate, endDate);
		
		System.out.println(launchDate);
		Assert.assertNull(launchDate);
	}

}
