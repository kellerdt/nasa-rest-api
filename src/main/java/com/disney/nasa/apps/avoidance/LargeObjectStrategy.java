package com.disney.nasa.apps.avoidance;

import java.util.List;

import com.disney.nasa.models.NasaApiResult.NearEarthObject;

/**
 * Counts the number of large asteroids that are close on a particular day.
 */
public class LargeObjectStrategy implements AsteroidAvoidanceStrategy {

	
	public static final String KILOS_KEY = "kilometers";
	public static final double DANGEROUS_DIA_KM = 0.5d;

	@Override
	public int countDangerousObjects(List<NearEarthObject> objects) {
		int count = 0;
		for(NearEarthObject neo : objects) {
			if(neo.estimated_diameter.get(KILOS_KEY).estimated_diameter_max > DANGEROUS_DIA_KM) {
				count++;
			}
		}
		return count;
	}
}
