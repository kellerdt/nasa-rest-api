package com.disney.nasa.apps.avoidance;

import java.util.List;

import com.disney.nasa.models.NasaApiResult.NearEarthObject;

/**
 * Interface to allow us to employ different algorithms for determining safety.
 */
public interface AsteroidAvoidanceStrategy {

	int countDangerousObjects(List<NearEarthObject> objects);
}
