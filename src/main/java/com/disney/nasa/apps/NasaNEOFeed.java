package com.disney.nasa.apps;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.disney.nasa.apps.avoidance.AsteroidAvoidanceStrategy;
import com.disney.nasa.apps.avoidance.LargeObjectStrategy;
import com.disney.nasa.models.LaunchDateData;
import com.disney.nasa.models.NasaApiResult;
import com.disney.nasa.models.NasaApiResult.NearEarthObject;

/**
 * Make calls to the NASA NEO feed to get information back about the incoming asteroids.
 * 
 * Example NASA Rest Call:
 * https://api.nasa.gov/neo/rest/v1/feed?start_date=2015-09-07&end_date=2015-09-08&api_key=DEMO_KEY
 */
@Component
public class NasaNEOFeed {

	private static final String URL =
			"https://api.nasa.gov/neo/rest/v1/feed?start_date=%s&end_date=%s&api_key=%s";
	private static final String KEY = "H7V98iNStSoR1hRBQJgIMHWee0W6Pf5qvfyVLk4X";
	
	private final AsteroidAvoidanceStrategy strategy = new LargeObjectStrategy();
	private final RestTemplate restTemplate = new RestTemplate();
	private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * Return the date of the safest day to launch.
	 * @param startDate - first allowable day
	 * @param endDate - last allowable day
	 * @return - String representation of the date
	 */
	public String findSafestDay(Date startDate, Date endDate) {
		if(startDate.after(endDate)) {
			return null;
		}
		
		String url = String.format(URL, dateFormat.format(startDate),
				dateFormat.format(endDate), KEY);
		ResponseEntity<NasaApiResult> response = null;
		try {
			response = restTemplate.getForEntity(url, NasaApiResult.class);
		} catch(RestClientException ex) {
			// If we have time we should apply retry logic.
		}

		String date = null;
		if(response != null) {
			NasaApiResult content = response.getBody();
			
			// Do the calculations to determine the correct day.
			int maxCount = 0;
			for(Map.Entry<String, List<NearEarthObject>> entry : content.nearEarthObjects.entrySet()) {
				int count = strategy.countDangerousObjects(entry.getValue());
				if(count > maxCount) {
					maxCount = count;
					date = entry.getKey();
				}
			}
		}
		return date;
	}
	
	/**
	 * This method would be used to add the NEO's information for that day into the object.
	 * I didn't get time to implement the stretch goal because I would need to do some additional
	 * modification to the POJO model objects in order to store the data from both the NEO rest
	 * service and into our REST object model.
	 * 
	 * @param data
	 */
	public void addNEOData(LaunchDateData data) {
		
	}
}
