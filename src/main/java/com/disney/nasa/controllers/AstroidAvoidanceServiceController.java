package com.disney.nasa.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.disney.nasa.apps.NasaNEOFeed;
import com.disney.nasa.models.LaunchDateData;

/**
 * The REST controllers job is to handle input formatting issues and HTTP responsibilities.
 */
@RestController
public class AstroidAvoidanceServiceController {

	@Autowired
	private NasaNEOFeed nasaNEOFeed;
	
	/**
	 * Remove default values after testing since both parameters should be required for this request.
	 * @param startDate - first possible day of launch (inclusive)
	 * @param endDate - last possible day of launch (inclusive)
	 * @return
	 */
	@RequestMapping(value="/nasa/launch/launchday", method=RequestMethod.GET, produces="Application/JSON")
	public LaunchDateData getLaunchDate(
			@RequestParam(value="startDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date startDate,
			@RequestParam(value="endDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date endDate) {
			
		String launchDate = nasaNEOFeed.findSafestDay(startDate, endDate);
		
		return new LaunchDateData(launchDate);
	}
}
