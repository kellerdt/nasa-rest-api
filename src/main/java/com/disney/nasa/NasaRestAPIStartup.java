package com.disney.nasa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Locating this test application in (com.disney.nasa) because the default ComponentScan will look in all
 * sub-directories allowing for easier project creation for a test application.
 */
@SpringBootApplication
public class NasaRestAPIStartup {

    public static void main(String[] args) {
        SpringApplication.run(NasaRestAPIStartup.class, args);
    }
}
