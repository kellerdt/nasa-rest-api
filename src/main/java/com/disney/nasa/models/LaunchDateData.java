package com.disney.nasa.models;

/**
 * Immutable POJO to store all data for the launch date.
 */
public class LaunchDateData {

	// Debated making this an actual Date object.
	// Would make it easier to compare dates to pick one. (Earliest / Latest)
	private final String date;
	
	public LaunchDateData(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}
}
