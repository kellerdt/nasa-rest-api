package com.disney.nasa.models;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object to store the returned results from the NEO NASA REST API call.
 */
public class NasaApiResult {
	
	  @JsonProperty("near_earth_objects")
	  public Map<String, List<NearEarthObject>> nearEarthObjects;

	  public static class NearEarthObject {
	    public String neo_reference_id;
	    public String name;
	    public Map<String, Diameter> estimated_diameter;
	  }

	  public static class Diameter {
	    public double estimated_diameter_max;
	  }
}
